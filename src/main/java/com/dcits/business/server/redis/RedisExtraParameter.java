package com.dcits.business.server.redis;

public class RedisExtraParameter {
	/**
	 * 连接redis的模式<br>
	 * local - 通过本地的redis-cli客户端去连接，可能获取信息比较慢
	 * remote - 通过telnet连接并获取信息	
	 */
	private String mode;
	private String linuxLoginUsername;
	private String linuxLoginPassword;
	private String linuxLoginPort = "22";
	/**
	 * 目标主机上redis-cli的绝对路径
	 */
	private String redisCliPath;
	
	public RedisExtraParameter(String mode, String linuxLoginUsername, String linuxLoginPassword, String linuxLoginPort,
			String redisCliPath) {
		super();
		this.mode = mode;
		this.linuxLoginUsername = linuxLoginUsername;
		this.linuxLoginPassword = linuxLoginPassword;
		this.linuxLoginPort = linuxLoginPort;
		this.redisCliPath = redisCliPath;
	}
	public RedisExtraParameter() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getLinuxLoginUsername() {
		return linuxLoginUsername;
	}
	public void setLinuxLoginUsername(String linuxLoginUsername) {
		this.linuxLoginUsername = linuxLoginUsername;
	}
	public String getLinuxLoginPassword() {
		return linuxLoginPassword;
	}
	public void setLinuxLoginPassword(String linuxLoginPassword) {
		this.linuxLoginPassword = linuxLoginPassword;
	}
	public String getLinuxLoginPort() {
		return linuxLoginPort;
	}
	public void setLinuxLoginPort(String linuxLoginPort) {
		this.linuxLoginPort = linuxLoginPort;
	}
	public String getRedisCliPath() {
		return redisCliPath;
	}
	public void setRedisCliPath(String redisCliPath) {
		this.redisCliPath = redisCliPath;
	}
	@Override
	public String toString() {
		return "RedisExtraParameter [mode=" + mode + ", linuxLoginUsername=" + linuxLoginUsername
				+ ", linuxLoginPassword=" + linuxLoginPassword + ", linuxLoginPort=" + linuxLoginPort
				+ ", redisCliPath=" + redisCliPath + "]";
	}
}	
