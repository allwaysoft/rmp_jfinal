package com.dcits.business.server.weblogic;

import java.util.HashMap;
import java.util.Map;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.annotation.JSONField;
import com.dcits.business.server.ViewServerInfo;
import com.dcits.business.server.weblogic.jmx.JMXWeblogicUtil;

public class WeblogicServer extends ViewServerInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String SERVER_TYPE_NAME = "weblogic";
	
	private static final Logger logger = Logger.getLogger(WeblogicServer.class);
	private static final Map<String, Object> alertThreshold = new HashMap<String, Object>();

	static {
		// TODO Auto-generated method stub
		alertThreshold.put(WeblogicMonitoringInfo.JVM_FREE_PERCENT, 10);
		alertThreshold.put(WeblogicMonitoringInfo.THREAD_PENDING_COUNT, 1);
		alertThreshold.put(WeblogicMonitoringInfo.JDBC_WAITING_FOR_CONNECTION_CURRENT_COUNT, 1);
	}

	
	/**
	 * weblogic的指定的连接
	 */
	@JSONField(serialize=false)
	private MBeanServerConnection connection;	
	/**
	 * 运行时状态
	 */
	@JSONField(serialize=false)
	private ObjectName serverRuntime;
	
	/**
	 * jvm状态
	 */
	@JSONField(serialize=false)
	private ObjectName jvmRuntime;
	
	/**
	 * jdbc状态
	 */
	@JSONField(serialize=false)
	private ObjectName jdbcRuntime;
	
	/**
	 * 线程状态
	 */
	@JSONField(serialize=false)
	private ObjectName threadPoolRuntime;
	
	/**
	 * 是否第一次获取信息
	 */
	@JSONField(serialize=false)
	private Boolean ifFirstGetInfo = true;
	
	public WeblogicServer() {
		super(new WeblogicMonitoringInfo());
		// TODO Auto-generated constructor stub
	}

	@Override
	public String connect() {
		// TODO Auto-generated method stub
		try {
			JMXWeblogicUtil.getConnection(this);
			this.getMonitoringInfo();
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("weblogic" + getHost() + ":" + getPort() + "JMX连接失败!", e);
			return e.getMessage() == null ? "服务器无法连接!" : e.getMessage();
		}
		
		return "true";
	}

	@Override
	public boolean disconect() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void getMonitoringInfo() {
		// TODO Auto-generated method stub
		try {
			JMXWeblogicUtil.getWeblogicInfo(this);
			this.connectStatus = "true";
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("weblogic" + getHost() + ":" + getPort() + "获取信息失败", e);
			this.connectStatus = "获取信息失败:" + e.getMessage() == null ? "服务器无法连接!" : e.getMessage();
		}
	}

	public MBeanServerConnection getConnection() {
		return connection;
	}

	public void setConnection(MBeanServerConnection connection) {
		this.connection = connection;
	}

	public ObjectName getServerRuntime() {
		return serverRuntime;
	}

	public void setServerRuntime(ObjectName serverRuntime) {
		this.serverRuntime = serverRuntime;
	}

	public ObjectName getJvmRuntime() {
		return jvmRuntime;
	}

	public void setJvmRuntime(ObjectName jvmRuntime) {
		this.jvmRuntime = jvmRuntime;
	}

	public ObjectName getJdbcRuntime() {
		return jdbcRuntime;
	}

	public void setJdbcRuntime(ObjectName jdbcRuntime) {
		this.jdbcRuntime = jdbcRuntime;
	}

	public ObjectName getThreadPoolRuntime() {
		return threadPoolRuntime;
	}

	public void setThreadPoolRuntime(ObjectName threadPoolRuntime) {
		this.threadPoolRuntime = threadPoolRuntime;
	}

	public Boolean getIfFirstGetInfo() {
		return ifFirstGetInfo;
	}

	public void setIfFirstGetInfo(Boolean ifFirstGetInfo) {
		this.ifFirstGetInfo = ifFirstGetInfo;
	}

}
